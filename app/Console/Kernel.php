<?php

namespace Newsltr\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Newsltr\Models\Application;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $applications = Application::all();

            foreach ($applications as $app) {
                Artisan::call('newsletter:send', [
                    '--filters'    => json_encode([['application_id', '=', $app->id]]),
                    '--view'       => $app->newsletter_view,
                    '--from_email' => $app->email,
                    '--from_name'  => $app->name,
                    '--subject'    => $app->name . " Newsletter",
                ]);
            }
        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
