<?php


namespace Newsltr\Facades;


use Illuminate\Support\Facades\Facade;
use Newsltr\Models\Application;

class ReportIncludesFacade extends Facade
{
    public static $possibleIncludes = [
        'active_subscribers',
        'inactive_subscribers',
        'failure_stats',
    ];

    // TODO : failure stats still missing implementation
    public static function get(Application $application, $includes)
    {
        $finalIncludesArray = [];

        if($includes == null)
            return $finalIncludesArray;

        foreach ($includes as $include) {
            $includeIsImplemented = method_exists(get_class(),$include);

            if($includeIsImplemented){
                $finalIncludesArray[] = self::$include($application);
            }
        }

        return $finalIncludesArray;
    }


    private static function active_subscribers(Application $application)
    {
        $subscribers = $application->newsletters()->where('active', true)->get();

        return [
            'active_subscribers' => [
                'total' => $subscribers->count(),
                'list' => $subscribers->map(function($value){
                    return [
                        'email' => $value->email,
                        'last_sent' => $value->last_sent
                    ];
                })
            ]
        ];
    }

    private static function inactive_subscribers(Application $application)
    {
        $subscribers = $application->newsletters()->where('active', false)->get();

        return [
            'inactive_subscribers' => [
                'total' => $subscribers->count(),
                'list' => $subscribers->map(function($value){
                    return [
                        'email' => $value->email,
                        'last_sent' => $value->last_sent
                    ];
                })
            ]
        ];
    }

}