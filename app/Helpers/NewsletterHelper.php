<?php

use Illuminate\Support\Facades\Artisan;

function execSendEmailCommand($application, $filtersArray, $sendToAllSubscribers = false)
{
    $commandOptions = [
        '--filters'    => json_encode($filtersArray),
        '--view'       => $application->newsletter_view,
        '--from_email' => $application->email,
        '--from_name'  => $application->name,
        '--subject'    => $application->name . " Newsletter",
    ];

    if ($sendToAllSubscribers)
        $commandOptions['--all_subscribers'] = $sendToAllSubscribers;

    try {
        Artisan::call('newsletter:send', $commandOptions);
    } catch (\Exception $exception) {
        return ['error' => true, 'msg' => 'Something went wrong while running command.', 'exception' => $exception->getMessage()];
    }
    return ['error' => false, 'msg' => 'Newsletter was sent successfully.', 'exception' => null];

}