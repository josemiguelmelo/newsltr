<?php

namespace Newsltr\Listeners;

use Carbon\Carbon;
use Newsltr\Events\NewsletterCommandExecutedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Newsltr\Models\DeliveryLog;

class NewsletterCommandExecutedEventListener
{

    public $logContent = [
        'newsletter_id'  => null,
        'exception'      => '',
        'when_date'      => null,
        'application_id' => null,
        'type'           => 'all',
        'delivered'      => true,
    ];

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SomeEvent $event
     * @return void
     */
    public function handle(NewsletterCommandExecutedEvent $event)
    {
        $this->logContent['delivered'] = ! $event->commandResult['error'];

        if ($event->type == NewsletterCommandExecutedEvent::$SPECIFIC_TYPE) {
            $this->logContent['newsletter_id'] = $event->newsletter->id;
            $this->logContent['application_id'] = $event->newsletter->application_id;
        } else {
            $this->logContent['application_id'] = $event->application->id;
        }

        $this->logContent['exception'] = $event->commandResult['exception'] == null ? '' : $event->commandResult['exception'];
        $this->logContent['type'] = $event->type;
        $this->logContent['when_date'] = Carbon::now();

        DeliveryLog::create($this->logContent);
    }
}
