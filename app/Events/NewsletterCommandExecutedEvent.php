<?php

namespace Newsltr\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use JoseMiguelMelo\Newsletter\Models\Newsletter;
use Newsltr\Models\Application;

class NewsletterCommandExecutedEvent
{
    use InteractsWithSockets, SerializesModels;

    public static $SPECIFIC_TYPE = 'specific';
    public static $ALL_TYPE = 'all';

    public $newsletter;
    public $type;
    public $commandResult;
    public $application;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($objectContent, $commandResult, $type)
    {
        if($type == NewsletterCommandExecutedEvent::$SPECIFIC_TYPE)
        {
            $this->newsletter = $objectContent;
            $this->application = null;
            $this->type = NewsletterCommandExecutedEvent::$SPECIFIC_TYPE;
        }
        elseif ($type = NewsletterCommandExecutedEvent::$ALL_TYPE)
        {
            $this->newsletter = null;
            $this->application = $objectContent;
            $this->type = NewsletterCommandExecutedEvent::$ALL_TYPE;
        }
        $this->commandResult = $commandResult;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
