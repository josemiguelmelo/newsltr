<?php
namespace Newsltr\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use JoseMiguelMelo\Newsletter\Models\Newsletter;
use Newsltr\Models\Application;
use Newsltr\Events\NewsletterCommandExecutedEvent;
use Newsltr\Http\Requests\NewsletterRequest;
use Newsltr\Models\DeliveryLog;
use Newsltr\Models\Failure;

class NewsletterController extends Controller
{
    public function index($email)
    {
        $newsletterSubscription = Newsletter::whereEmail($email)->first();

        return response()->json(
            [
                'newsletter_subscription' => $newsletterSubscription,
            ]);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $application = Application::where('api_token', $input['api_token'])->first();
        if ( ! $application)
            return response()->json(
                [
                    'error' => true,
                    'msg'   => 'Application token not valid.',
                ]
            );


        if ( ! isset($input['email']))
            return response()->json(
                [
                    'error' => true,
                    'msg'   => 'Email required',
                ]
            );


        try {
            $newsletter = Newsletter::create([
                                                 'email'     => $input['email'],
                                                 'active'    => true,
                                                 'last_sent' => \Carbon\Carbon::now(),
                                             ]);
            $application->newsletters()->save($newsletter);
        } catch (QueryException $exception) {
            return response()->json(['error' => true, 'msg' => 'An exception was thrown while creating the newsletter subscription.']);
        }

        return response()->json(
            [
                'error'                   => false,
                'newsletter_subscription' => $newsletter,
            ]);
    }

    public function sendToAll(NewsletterRequest $request)
    {
        if ( ! $request->checkApplication())
            return response()->json(['error' => true, 'msg' => 'Application not found.']);

        // Run Send Newsletter Command
        $sendNewsletterResult = execSendEmailCommand($request->application, [['application_id', '=', $request->application->id]], true);

        // Fire event after receiving result from command
        event(new NewsletterCommandExecutedEvent($request->application, $sendNewsletterResult, NewsletterCommandExecutedEvent::$ALL_TYPE));

        // Update response according to result given by command
        $request->updateResponse($sendNewsletterResult);

        // Return response
        return $request->sendResponse();
    }

    public function sendToEmail(NewsletterRequest $request, $email)
    {
        if ( ! $request->checkApplication())
            response()->json(['error' => true, 'msg' => 'Application not found.']);

        $newsletter = Newsletter::whereEmail($email)
            ->where('application_id', $request->application->id)
            ->first();
        if ( ! $newsletter)
            return response()->json(['error' => true, 'msg' => 'Subscriber email not found.']);

        // Make subscriber active if he is inactive.
        $newsletterMessage = "";
        if ( ! $newsletter->active)
            $newsletterMessage = "\nSubscriber became active.";

        $newsletter->active = true;
        $newsletter->last_sent = Carbon::yesterday();
        $newsletter->save();

        // Send newsletter command filters
        $filtersArray = [
            ['application_id', '=', $request->application->id],
            ['email', '=', $email],
        ];

        // Send newsletter command
        $sendNewsletterResult = execSendEmailCommand($request->application, $filtersArray);

        // Fire event after receiving command result
        event(new NewsletterCommandExecutedEvent($newsletter, $sendNewsletterResult, NewsletterCommandExecutedEvent::$SPECIFIC_TYPE));

        $request->updateResponse($sendNewsletterResult);

        // Add information to message when subscriber became active
        if ( ! $request->responseJSON['error'])
            $request->responseJSON['msg'] .= $newsletterMessage;

        return $request->sendResponse(true);
    }

    public function destroy($email)
    {
        try {
            Newsletter::where('email', $email)->update(['active' => false]);
        } catch (\Exception $exception) {
            return response()->json(['error' => true, 'msg' => 'An exception was thrown while destroying the newsletter subscription..', 'exception' => $exception->getMessage()]);
        }
        return response()->json(['error' => false, 'msg' => 'Newsletter destroyed successfully.']);
    }

    public function activate($email)
    {
        try {
            Newsletter::where('email', $email)->update(['active' => true]);
        } catch (\Exception $exception) {
            return response()->json(['error' => true, 'msg' => 'An exception was thrown while destroying the newsletter subscription..', 'exception' => $exception->getMessage()]);
        }
        return response()->json(['error' => false, 'msg' => 'Newsletter activated successfully.']);
    }

    public function generate()
    {
        $newsletter = factory(Newsletter::class)->make();
        $newsletter->save();
        return response()->json($newsletter);
    }
}
