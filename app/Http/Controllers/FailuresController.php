<?php

namespace Newsltr\Http\Controllers;

use Newsltr\Http\Requests\DeliveryLogAPIRequest;

class FailuresController extends Controller
{

    public function deliveryLog(DeliveryLogAPIRequest $request, $appId)
    {
        // Set request application Id
        $request->appId = $appId;

        // Handle request and return response
        return $request->handleRequest();
    }


}
