<?php

namespace Newsltr\Http\Controllers;

use Illuminate\Http\Request;
use Newsltr\Models\Application;
use Newsltr\Transformers\ReportTransformer;

class ReportController extends Controller
{
    public function retrieveInformation(Request $request)
    {
        $application = Application::find($request->get('application_id'));

        $includeSections = $request->get('includes');

        $response = (new ReportTransformer)->transformReport($application, $includeSections);

        return response()->json($response);
    }
}
