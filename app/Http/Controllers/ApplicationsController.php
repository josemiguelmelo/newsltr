<?php

namespace Newsltr\Http\Controllers;

use Newsltr\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicationsController extends Controller
{
    public function index()
    {
        if ( ! Auth::check())
            return response()->json(['error' => true, 'msg' => 'Please login to access']);

        return response()->json(['error' => false, 'applications' => Auth::user()->applications]);
    }

    public function show($id)
    {
        \Log::info("Application id = " . $id);
        if ( ! Auth::check())
            return response()->json(['error' => true, 'msg' => 'Please login to access']);

        return response()->json(
            [
                'error'       => false,
                'application' => Auth::user()->applications()->where('id', $id)->first(),
            ]);
    }

    public function create()
    {
        return view('applications.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|unique:applications,name|min:3',
            'email' => 'required|unique:applications,email|email',
        ]);

        $defaultFrequency = (($request->has('auto_send') == true) ? $request->get('frequency') : -1);

        $application = Auth::user()->applications()->create(
            [
                'name'              => $request->get('name'),
                'email'             => $request->get('email'),
                'auto_send'         => $request->has('auto_send'),
                'default_frequency' => $defaultFrequency,
                'api_token'         => str_random(30),
            ]);

        if ( ! $request->ajax())
            return redirect()->route('home')->with('confirmationMessage', 'Application added successfully');

        return response()->json(
            [
                'error'       => false,
                'msg'         => 'Application added successfully.',
                'application' => $application,
            ]);
    }

    public function update(Request $request, $applicationId)
    {
        \Log::info($request->all());
        $application = Auth::user()->applications()->where('id', $applicationId)->first();

        if ( ! $application)
            return response()->json(
                [
                    'error' => true,
                    'msg'   => 'Application not found.',
                ]);

        $this->validate($request, [
            'name'              => 'required|min:3|unique:applications,name,' . $application->id,
            'email'             => 'required|email|unique:applications,email,' . $application->id,
            'auto_send'         => 'required|boolean',
            'default_frequency' => 'in:"daily","weekly","monthly"',
        ]);

        $application->name = $request->has('name') ? $request->get('name') : $application->name;
        $application->email = $request->has('email') ? $request->get('email') : $application->email;
        $application->auto_send = $request->has('auto_send') ? $request->get('auto_send') : $application->auto_send;
        $application->default_frequency = $request->has('default_frequency') ? $request->get('default_frequency') : $application->default_frequency;

        $application->save();

        return response()->json(
            [
                'error'       => false,
                'msg'         => 'Application updated successfully.',
                'application' => $application,
            ]);
    }

    public function destroy($application)
    {
        if (Application::destroy($application))
            return response()->json(
                [
                    'error' => false,
                    'msg'   => 'Application deleted successfully.',
                ]);

        // Application not deleted.
        return response()->json(
            [
                'error' => true,
                'msg'   => 'Application could not be deleted.',
            ]);
    }

    public function updateNewsletter(Request $request)
    {
        if ( ! $request->has('application_id') || ! $request->has('view'))
            return response()->json(['error' => true, 'msg' => 'Missing param.']);

        $input = $request->all();

        $application = Application::find($input['application_id']);
        if ( ! $application)
            return response()->json(['error' => true, 'msg' => 'Application not found.']);

        $application->newsletter_view = $input['view'];
        $application->save();

        return response()->json(['error' => false, 'msg' => 'View updated successfully.']);
    }


    /**
     * @param $applicationId Application id sent on url
     * @return \Illuminate\Http\JsonResponse List of subscribers of application with id $applicationId
     */
    public function subscribers($applicationId)
    {
        $subscribers = \Newsltr\Models\Application::find($applicationId)->newsletters;
        return response()->json($subscribers);
    }


    public function newsletterPage(Request $request)
    {
        $website = $request->get('website');
        if (filter_var($website, FILTER_VALIDATE_URL)) {
            $webPage = file_get_contents($website);
            if ( ! $this->is_utf8($webPage))
                $webPage = utf8_decode($webPage);
            return response()->json(['error' => false, 'html' => $webPage]);
        }
        return response()->json(['error' => true, 'msg' => 'Not valid website']);
    }


    private function is_utf8($string)
    {
        return (mb_detect_encoding($string, 'UTF-8', true) == 'UTF-8');
    }
}
