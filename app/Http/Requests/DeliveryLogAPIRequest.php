<?php

namespace Newsltr\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Newsltr\Models\Application;
use Newsltr\Models\DeliveryLog;

class DeliveryLogAPIRequest extends FormRequest
{

    public $appId   = null;
    public $filters = [];
    public $groups = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function monthlyLog()
    {
        $applicationFailuresLastYear = failuresSinceBeginOfYear($this->appId);
        $applicationSuccessfulLastYear = successfulSinceBeginOfYear($this->appId);

        $applicationFailuresLastYear = $applicationFailuresLastYear->groupBy(function ($item) {
            $date = Carbon::parse($item['when_date']);
            return $date->month;
        });

        $applicationSuccessfulLastYear = $applicationSuccessfulLastYear->groupBy(function ($item) {
            $date = Carbon::parse($item['when_date']);
            return $date->month;
        });

        return response()->json(
            [
                'error'    => false,
                'failures' => $applicationFailuresLastYear,
                'success'  => $applicationSuccessfulLastYear,
            ]);
    }

    public function handleRequest()
    {

        if ( ! Application::find($this->appId))
            return response()->json(
                [
                    'error' => true,
                    'msg'   => 'Invalid application.',
                ]);


        if ($this->has('year'))
            $this->filters['year'] = $this->get('year');

        if ($this->has('groups')) {
            $this->groups = json_decode($this->get('groups'));
        }

        return $this->fetchResults();


    }

    private function applyFiltersToCollection($collection)
    {
        foreach ($this->filters as $filterKey => $filterVal) {
            $collection = $collection
                ->filter(function($collection) use ($filterKey, $filterVal) {
                    if ($filterKey == "year") {
                        $date = Carbon::parse($collection['when_date']);
                        return $date->year == $filterVal;
                    }
                    return $collection[$filterKey] == $filterVal;
                });
        }
        return $collection;
    }

    private function applyGroupToCollection($collection, $group)
    {
        return $collection->groupBy(function ($item) use ($group) {
            if ($group == "year") {
                $date = Carbon::parse($item['when_date']);
                return $date->year;
            }
            if ($group == "month") {
                $date = Carbon::parse($item['when_date']);
                return $date->month;
            }
            return $item[$group];
        });

    }

    public function fetchResults()
    {
        $finalResult = [];

        foreach ($this->groups as $group) {
            $finalResult[$group] = DeliveryLog::where('application_id', $this->appId)
                ->get();

            $finalResult[$group] = $this->applyFiltersToCollection($finalResult[$group]);

            $finalResult[$group] = $this->applyGroupToCollection($finalResult[$group], $group);
        }

        return $finalResult;
    }
}
