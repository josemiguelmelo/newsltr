<?php

namespace Newsltr\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Newsltr\Models\Application;
use Newsltr\Models\DeliveryLog;

class NewsletterRequest extends FormRequest
{
    public $responseJSON = [
        'error'     => false,
        'msg'       => '',
        'exception' => null,
    ];
    public $application;

    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function checkApplication()
    {
        $this->application = Application::find($this->get('application_id'));

        if ( ! $this->application)
            return false;

        return $this->application;
    }

    public function updateResponse($responseArray)
    {
        $this->responseJSON['error'] = isset($responseArray['error']) ? $responseArray['error'] : $this->responseJSON['error'];
        $this->responseJSON['exception'] = isset($responseArray['exception']) ? $responseArray['exception'] : $this->responseJSON['exception'];
        $this->responseJSON['msg'] = isset($responseArray['msg']) ? $responseArray['msg'] : $this->responseJSON['msg'];
    }

    public function sendResponse()
    {
        return response()->json($this->responseJSON);
    }
}
