<?php

namespace Newsltr\Models;

use Illuminate\Database\Eloquent\Model;


class DeliveryLog extends Model
{
    public static $TYPE_ALL      = 'all';
    public static $TYPE_SPECIFIC = 'specific';

    public    $timestamps = false;

    protected $table      = "newsletter_delivery_log";
    protected $fillable   = [
        'exception', 'when_date', 'newsletter_id', 'application_id', 'type', 'delivered',
    ];

    public function newsletter()
    {
        return $this->hasMany('\JoseMiguelMelo\Newsletter\Models\Newsletter', 'newsletter_id');
    }

    public function application()
    {
        return $this->hasMany('\JoseMiguelMelo\Newsletter\Models\Newsletter', 'newsletter_id');
    }


}
