<?php

namespace Newsltr\Models;

use Illuminate\Database\Eloquent\Model;


class Application extends Model
{
    //
    protected $fillable = [
        'name', 'email', 'auto_send', 'default_frequency', 'api_token'
    ];

    public function user()
    {
        return $this->belongsTo('Newsltr\Models\User', 'user_id');
    }

    public function newsletters()
    {
        return $this->hasMany('\JoseMiguelMelo\Newsletter\Models\Newsletter', 'application_id');
    }


    public function getDefaultFrequencyAttribute($value)
    {
        switch ($value)
        {
            case 0: return 'daily';
            case 1: return 'weekly';
            case 2: return 'monthly';
            default: return 'daily';
        }
    }
    public function setDefaultFrequencyAttribute($value)
    {
        switch ($value)
        {
            case 'daily':
                $this->attributes['default_frequency'] = 0;
                break;
            case 'weekly':
                $this->attributes['default_frequency'] = 1;
                break;
            case 'monthly':
                $this->attributes['default_frequency'] = 2;
                break;
            default:
                $this->attributes['default_frequency'] = 0;
                break;
        }
    }
}
