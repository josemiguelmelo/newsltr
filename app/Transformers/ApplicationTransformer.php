<?php

namespace Newsltr\Transformers;


use Newsltr\Models\Application;

class ApplicationTransformer
{
    public function transformApplication(Application $application)
    {
        return [
            'name'        => $application->name,
            'email'       => $application->email,
            'last_update' => $application->updated_at,
            'created_at'  => $application->created_at,
            'frequency'   => $application->default_frequency,
            'admin'       => [
                'name'  => $application->user->name,
                'email' => $application->user->email,
            ],
        ];
    }
}