<?php

namespace Newsltr\Transformers;


use Newsltr\Models\Application;
use Newsltr\Facades\ReportIncludesFacade;

class ReportTransformer
{
    public function transformReport(Application $application, $includes)
    {
        $optionalInformation = ReportIncludesFacade::get($application, $includes);

        return [
            'application' => (new ApplicationTransformer)->transformApplication($application),
            'includes'    => $optionalInformation,
        ];
    }
}