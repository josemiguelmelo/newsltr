<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApplicationToNewsletter extends Migration
{


    public function up()
    {
        Schema::table('newsletters', function ($table) {
            $table->integer('application_id')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')
                ->on('applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newsletters', function ($table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
        });
    }
}
