<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletter_delivery_log', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('newsletter_id')->unsigned()->nullable();
            $table->foreign('newsletter_id')->references('id')->on('newsletters');

            $table->integer('application_id')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');


            $table->boolean('delivered');
            $table->enum('type',['all', 'specific']);
            $table->text('exception');
            $table->dateTime('when_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newsletter_delivery_log', function ($table) {
            $table->dropForeign(['newsletter_id']);

            $table->dropForeign(['application_id']);
        });

        Schema::dropIfExists('newsletter_delivery_log');
    }
}
