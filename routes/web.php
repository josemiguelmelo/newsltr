<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['prefix' => 'api'], function () {
    \JoseMiguelMelo\Newsletter\Models\Newsletter::routes();

    Route::get('application/{id}/subscribers', ['uses' => 'ApplicationsController@subscribers']);
    Route::resource('application', 'ApplicationsController');
    Route::post('application/send_all', ['uses' => 'NewsletterController@sendToAll']);

    Route::get('application/{id}/delivery_log', ['uses' => 'FailuresController@deliveryLog']);

    Route::post('application/report', ['uses' => 'ReportController@retrieveInformation']);
    Route::post('application/update_view', ['uses' => 'ApplicationsController@updateNewsletter']);

    Route::post('newsletter/send/{email}', ['uses' => 'NewsletterController@sendToEmail']);
    Route::post('newsletter/activate/{email}', ['uses' => 'NewsletterController@activate']);
});



Auth::routes();

Route::resource('application', 'ApplicationsController');

Route::get('newsletters/subscribe', function () {
    return view('subscribe');
});

Route::get('newsletter_page', ['uses' => 'ApplicationsController@newsletterPage']);

Route::get('/', [
    'as' => 'home', 'uses' => function () {
        if(\Illuminate\Support\Facades\Auth::check())
            return response()->redirectTo('/app/');

        return response()->view('home');
    }]);

Route::get('/{catchall?}', function () {
    return response()->view('home');
})->where('catchall', '(.*)')->middleware('auth');