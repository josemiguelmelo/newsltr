/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */


var Vue = require('vue');
var VueRouter = require('vue-router');
var Clipboard = require('clipboard');
new Clipboard('.btn');

require('jspdf');


require('./bootstrap');
require('./table2excel.min');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */


Vue.use(VueRouter);

const Welcome = require('./components/Welcome.vue');
const Dashboard = require('./components/Dashboard.vue');
const CreateApplication = require('./components/applications/Create.vue');
const ApplicationDashboard = require('./components/applications/ApplicationDashboard.vue');
const ApplicationSubscribers = require('./components/applications/subscribers/List.vue');
const ApplicationReport = require('./components/applications/reports/ConfigurationPage.vue');
const DeliveryReportDashboard = require('./components/applications/delivery/Dashboard.vue');

const routes = [
    {path: '/', component: Welcome},
    {path: '/app/', component: Dashboard},
    {path: '/app/home', component: Dashboard},
    {path: '/app/application/create', component: CreateApplication},
    {path: '/app/application/:id', component: ApplicationDashboard},
    {path: '/app/application/:id/subscribers', component: ApplicationSubscribers},
    {path: '/app/application/:id/report', component: ApplicationReport},
    {path: '/app/application/:id/delivery-report', component: DeliveryReportDashboard}
]

var router = new VueRouter({
    mode: 'history',
    routes: routes
});


require('./directives/string');

require('./helper/url');
require('./helper/ApplicationAPI');
require('./helper/SubscribersAPI');
require('./helper/Maths');


const app = new Vue({
    router
}).$mount("#app");

