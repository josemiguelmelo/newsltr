export default Vue.directive('firstLetterUppercase', function (el, binding) {
    var contentString = el.innerHTML;
    var firstChar = contentString.charAt(0);
    el.innerHTML = firstChar.toUpperCase() + contentString.slice(1);
});
