export default Vue.mixin({

    methods: {
        twoDecimalPlaces : function(number)
        {
            return Math.round(number * 100) / 100
        }
    }
});