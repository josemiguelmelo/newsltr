export default Vue.mixin({
    data(){
        return{
            baseURL : '/',
            saving : false,
            application : {id : -1},
            error: null,
            postResult : null
        }
    },
    methods: {
        getApplication : function(id) {
            return this.$http.get(this.baseURL + 'application/' + id);
        },

        getApplicationsList : function(){
            return this.$http.get(this.baseURL + 'application');
        },

        createApplication : function(application)
        {
            return this.$http.post('/application', application)
        },

        saveApplication : function(application)
        {
            return this.$http.put(this.baseURL + 'application/' + application.id, application);
        },

        deleteApplication : function(application)
        {
            return this.$http.delete(this.baseURL + 'application/' + application.id);
        },


        getWebsitePreview : function(application){
            return this.$http.get(this.baseURL + 'newsletter_page?website=' + application.newsletter_view);
        },

        updateApplicationNewsletterContent : function(params)
        {
            return this.$http.post(this.baseURL + 'application/update_view', params);
        },

        getReportInformation : function(application, includes)
        {
            var params = {
                application_id : application.id,
                includes : includes
            };
            return this.$http.post(this.baseURL + 'application/report', params);
        }
    }
});