export default Vue.mixin({
    methods: {
        validURL : function(str){
            var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                '(\\#[-a-z\\d_]*)?$','i');

            return pattern.test(str);
        },

        previewURLContent : function(iframeID, content)
        {
            document.getElementById(iframeID).src = "data:text/html;charset=utf-8," + escape(content);

        }
    }
});