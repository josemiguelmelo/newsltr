export default Vue.mixin({
    data(){
        return{
            baseURL : '/api/',
            saving : false,
            application : {id : -1},
            error: null,
            postResult : null
        }
    },
    methods: {
        deactivateSubscriber : function(subscriber)
        {
            return this.$http.delete(this.baseURL + 'newsletter/' + subscriber.email);
        },

        activateSubscriber : function(subscriber)
        {
            return this.$http.post(this.baseURL + 'newsletter/activate/' + subscriber.email);
        },

        sendNewsletterToSubscriber : function(subscriber, application)
        {
            return this.$http.post(this.baseURL + 'newsletter/send/' + subscriber.email, {application_id : subscriber.application_id});
        },
    }
});