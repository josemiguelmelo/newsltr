@if(session('confirmationMessage'))
    <div class="alert alert-success">
        <div>{{ session('confirmationMessage') }}</div>
    </div>
@endif