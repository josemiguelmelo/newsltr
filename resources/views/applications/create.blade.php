@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">New Application</div>

                    <div class="panel-body">
                        @include('errors.form-errors')

                        <form method="post" action="{{ route('application.store') }}">
                            <input type="text" value="{{ csrf_token() }}" name="_token" hidden/>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Your application name">
                            </div>

                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Email used to send newsletter">
                            </div>

                            <div class="form-group">
                                <label>Auto Send Newsletter</label>
                                <div class="material-switch pull-right">
                                    <input id="auto-send" name="auto-send" type="checkbox" value="true"/>
                                    <label for="auto-send" class="label-success"></label>
                                </div>
                            </div>

                            <div class="form-group" id="frequency-selection" hidden>
                                <label>Frequency</label>
                                <select class="form-control" name="frequency">
                                    <option value="daily">Daily</option>
                                    <option value="weekly">Weekly</option>
                                    <option value="monthly">Monthly</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.getElementById("auto-send").onclick = function () {
            if (this.checked) {
                document.getElementById("frequency-selection").style.display = 'block';
            } else {
                document.getElementById("frequency-selection").style.display = 'none';
            }
        };
    </script>
@endsection
